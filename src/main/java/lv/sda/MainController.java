package lv.sda;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import lv.sda.entity.Client;
import lv.sda.repository.ClientRepository;


import java.util.List;

public class MainController {

    @FXML
    private TableView<Client> clientsTable;

    @FXML
    TextField clientFirstName;

    @FXML
    private TextField clientLastName;

    @FXML
    private TextField clientEmail;

    @FXML
    private TableColumn<Client, Integer> clientIdColumn;

    @FXML
    private TableColumn<Client, String> clientFirstNameColumn;

    @FXML
    private TableColumn<Client, String> clientLastNamecolumn;

    @FXML
    private TableColumn<Client, String> clientEmailColumn;


    @FXML
    void clientsTableClicked(MouseEvent event) {
        Client selected = clientsTable.getSelectionModel().getSelectedItem();

        if (null == selected) return;

        clientFirstName.setText(selected.getName());
        clientLastName.setText(selected.getSurname());
        clientEmail.setText(selected.getEmail());
    }

    @FXML
    void fetchTrainers(ActionEvent event) {
        clientIdColumn.setCellValueFactory(new PropertyValueFactory<>("clientId"));
        clientFirstNameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        clientLastNamecolumn.setCellValueFactory(new PropertyValueFactory<>("surname"));
        clientEmailColumn.setCellValueFactory(new PropertyValueFactory<>("email"));
        clientsTable.getItems().clear();
        List<Client> client = ClientRepository.findAll();
        clientsTable.getItems().addAll(client);

    }

    @FXML
    protected void addTrainer(ActionEvent event) {
        Client p = new Client();
        p.setName(clientFirstName.getText());
        p.setSurname(clientLastName.getText());
        p.setEmail(clientEmail.getText());
        ClientRepository.save(p);
        System.out.println("Trainer saved");
    }

    @FXML
    protected void updateTrainer(ActionEvent event) {
        Client selected = clientsTable.getSelectionModel().getSelectedItem();
        Client updatedTrainer = new Client();
        updatedTrainer.setClientId(selected.getClientId());
        updatedTrainer.setName(clientFirstName.getText());
        updatedTrainer.setSurname(clientLastName.getText());
        updatedTrainer.setEmail(clientEmail.getText());
        ClientRepository.update(updatedTrainer);
        System.out.println("Client updated");
        fetchTrainers(new ActionEvent());

    }

    @FXML
    protected void deleteTrainer(ActionEvent event) {
        Client selected = clientsTable.getSelectionModel().getSelectedItem();
        Client toDelete = new Client();
        toDelete.setClientId(selected.getClientId());
        ClientRepository.delete(toDelete);
        fetchTrainers(new ActionEvent());
        System.out.println("Trainer deleted");
    }
}
