package lv.sda.repository;

import lv.sda.HibernateUtils;
import lv.sda.entity.Bet;
import lv.sda.entity.Client;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class BetRepository {

    public static List<Bet> findAll() {
        Session session = HibernateUtils.getSessionFactory().openSession();
        List<Bet> a = session.createQuery("FROM Bet a", Bet.class).getResultList();
        session.close();
        return a;
    }

    public static Bet findByID (Integer id) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Bet c = session.find(Bet.class, id);
        session.close();
        return c;
    }
    public static Bet save(Bet bet) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(bet);
        transaction.commit();
        session.close();
        return bet;
    }

    public static Bet update(Bet bet) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(bet);
        transaction.commit();
        session.close();
        return bet;
    }

    public static void delete(Bet Bet) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(Bet);
        transaction.commit();
        session.close();
    }

}
