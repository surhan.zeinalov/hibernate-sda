package lv.sda.repository;

import lv.sda.HibernateUtils;
import lv.sda.entity.Bet;
import lv.sda.entity.Client;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class ClientRepository {

    public static Client findByNameLastname(String name, String surname) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Client client = new Client();
        client.setName(name);
        client.setSurname(surname);
        Client c = session.find(Client.class, client);
        session.close();
        return c;
    }

    public static Client findByID (Integer id) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Client c = session.find(Client.class, id);
        session.close();
        return c;
    }

    public static Client save(Client Client) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(Client);
        transaction.commit();
        session.close();
        return Client;
    }

    public static Client update(Client Client) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(Client);
        transaction.commit();
        session.close();
        return Client;
    }

    public static void delete(Client Client) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(Client);
        transaction.commit();
        session.close();
    }

    public static List<Client> findAll() {
        Session session = HibernateUtils.getSessionFactory().openSession();
        List<Client> a = session.createQuery("FROM Client a", Client.class).getResultList();
        session.close();
        return a;
}

}
