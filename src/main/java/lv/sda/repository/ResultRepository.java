package lv.sda.repository;

import lv.sda.HibernateUtils;
import lv.sda.entity.Bet;
import lv.sda.entity.Client;
import lv.sda.entity.Result;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class ResultRepository {

    public static List<Result> findAll() {
        Session session = HibernateUtils.getSessionFactory().openSession();
        List<Result> a = session.createQuery("FROM Result a", Result.class).getResultList();
        session.close();
        return a;
    }

    public static Result findByID (Integer id) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Result c = session.find(Result.class, id);
        session.close();
        return c;
    }

    public static Result save(Result result) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(result);
        transaction.commit();
        session.close();
        return result;
    }

    public static Result update(Result result) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(result);
        transaction.commit();
        session.close();
        return result;
    }

}
