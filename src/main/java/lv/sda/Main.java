package lv.sda;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import lv.sda.entity.Result;
import lv.sda.repository.ResultRepository;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("interface.fxml"));
        primaryStage.setTitle("SDA demo app");
        primaryStage.setScene(new Scene(root, 610, 410));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
