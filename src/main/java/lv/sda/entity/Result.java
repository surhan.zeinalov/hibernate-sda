package lv.sda.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "asa_results")
public class Result {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "resultId")
    private Integer resultId;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="clientId", nullable=false)
    private Client client;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="betId", nullable=false)
    private Bet bet;

    public Integer getResultId() {
        return resultId;
    }

    public void setResultId(Integer resultId) {
        this.resultId = resultId;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Bet getBet() {
        return bet;
    }

    public void setBet(Bet bet) {
        this.bet = bet;
    }
}
