package lv.sda;

import lv.sda.entity.Bet;
import lv.sda.entity.Client;
import lv.sda.repository.BetRepository;
import lv.sda.repository.ClientRepository;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class BetRepositoryTest {

    @Before
    public void init() { }


    @Test
    public void testClientSaveAndUpdate() {
        Bet p = new Bet();
        p.setBetAmount(23d);
        p.setDisciplineName("Soccer");
        BetRepository.save(p);
        assertNotNull(p.getBetId());
        p.setDisciplineName("HOT KEY");
        BetRepository.update(p);
        Bet bet = BetRepository.findByID(p.getBetId());
        assertThat(bet.getDisciplineName(), is("HOT KEY"));
    }

    @Test
    public void testDelete() {
        Bet p = new Bet();
        p.setBetAmount(23d);
        p.setDisciplineName("Soccer");
        BetRepository.save(p);
        assertNotNull(p.getBetId());
        Integer id = p.getBetId();
        Bet p1 = new Bet();
        p1.setBetId(id);
        BetRepository.delete(p1);
        assertNull(ClientRepository.findByID(id));
    }

    @Test
    public  void testFindAll() {
        List<Bet> bets = BetRepository.findAll();
        assertNotNull(bets);
        assertTrue(bets.size()>0);
    }

}
