package lv.sda;

import lv.sda.entity.Bet;
import lv.sda.entity.Client;
import lv.sda.entity.Result;
import lv.sda.repository.ClientRepository;
import lv.sda.repository.ResultRepository;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class ClientRepositoryTest {

    @Before
    public void init() { }

    @Test
    public void testFindById() {
        Client client = ClientRepository.findByID(2);
        assertThat(client.getClientId(), is(client));
    }

    @Test
    public void testClientSaveAndUpdate() {
        Client p = new Client();
        p.setName("Surhan");
        p.setSurname("Zeinalov");
        p.setEmail("surhan@monokelberlin.de");
        ClientRepository.save(p);
        assertNotNull(p.getClientId());
        p.setEmail("surhan.zeinalov@gmail.com");
        ClientRepository.update(p);
        Client client = ClientRepository.findByID(p.getClientId());
        assertThat(client.getEmail(), is("surhan.zeinalov@gmail.com"));
    }

    @Test
    public void testDelete() {
        Client p = new Client();
        p.setName("Surhan");
        p.setSurname("Zeinalov");
        p.setEmail("surhan@monokelberlin.de");
        ClientRepository.save(p);
        assertNotNull(p.getClientId());
        Integer id = p.getClientId();
        Client p1 = new Client();
        p1.setClientId(id);
        ClientRepository.delete(p1);
        assertNull(ClientRepository.findByID(id));
    }

    @Test
    public  void testFindAll() {
        List<Client> clients = ClientRepository.findAll();
        assertNotNull(clients);
        assertTrue(clients.size()>0);
    }

}
