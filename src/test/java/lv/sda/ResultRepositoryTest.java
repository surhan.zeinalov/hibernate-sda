package lv.sda;

import lv.sda.entity.Bet;
import lv.sda.entity.BetResult;
import lv.sda.entity.Client;
import lv.sda.entity.Result;
import lv.sda.repository.BetRepository;
import lv.sda.repository.ClientRepository;
import lv.sda.repository.ResultRepository;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class ResultRepositoryTest {

    @Before
    public void init() { }

    @Test
    public void testClientSaveAndUpdate() {
        Client c = new Client();
        c.setName("Surhan");
        c.setSurname("Zeinalov");
        c.setEmail("surhan.zeinalov@gmail.com");
        Bet c1 = new Bet();
        c1.setBetAmount(23d);
        c1.setDisciplineName("Soccer");
        Result p = new Result();
        p.setClient(c);
        p.setBet(c1);
        ResultRepository.save(p);
        assertNotNull(p.getResultId());
        c1.setBetAmount(34d);
        ResultRepository.update(p);
        Result result = ResultRepository.findByID(p.getResultId());
        assertThat(result.getBet().getBetAmount(), is(34d));
    }


    @Test
    public  void testFindAll() {
        List<Result> results = ResultRepository.findAll();
        assertNotNull(results);
        assertTrue(results.size()>0);
    }
}
